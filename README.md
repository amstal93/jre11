# Introduction

This project creates a docker image with JRE 11 installed installed on Debian Buster.

The image can be used to run applications using java.

This repository is mirrored to https://gitlab.com/sw4j-net/jre11
